# Expressance

A very simple framework that extends expressJS and is using instance-based route.

## Installation

For `npm`:

```bash
npm install @letumfalx/expressance
```

For `yarn`:

```
yarn add @letumfalx/expressance
```

## API

### Kernel

- `app: Express` - The express instance.

- `server: Server` - The http server for the Kernel.

- `addMiddleware (middleware: RequestHandler): this` - Add an express middleware as a global middleware.

- `addRoute (route: Route): this` - Add a route.

- `cookieParser (secret: String|String[], options?: CokkieParseOptions): this` - Add the cookie-parser as a global middleware.

- `cors (options?: CorsOptions): this` - Add the cors middleware as a global middleware.

- `json (options: OptionsJson): this` - Add the json request body parser middleware as a global middleware.

- `raw (options: OptionsRaw): this` - Add the raw request body parser middleware as a global middleware.

- `start (...args): Server` - Starts the server. This will just pass the given arguments to the underlying server's listen method.

- `text (options: OptionsText): this` - Add the text requet body parser middleware as a global middleware.

- `trimRequest (type: 'all'|'body'|'param'|'query'): this` - Add the trim-request middleware as a global middleware.

- `urlencoded (options: OptionsUrlencoded): this` - Add the urlencoded request body parser as a global middleware.


### Route

- `method (): 'get'|'post'|'put'|'patch'|'delete'` - **Required**. Should return the method to be used by the route.

- `middlewares (): RequestHandler[]` - *Optional*. Should return the list of middleware to be bound to the route.

- `path (): String` - **Required**. Should return the path where this route will be bound.

- `handle: RequestHandler` - **Required**. The main handler for the incoming request.

- `static create (modifier: (route: Route) => void): Route` - Create a new generic route that will be modified by the given modifier callback.

## Usage

### Route

#### Creating a Route Class

```js
const Route = require('@letumfalx/expressance/Route');
const validateId = require('./middlewares/validateId');

class UserRoute extends Route {
  method () {
    return 'get';
  }

  middlewares () {
    return [
      validateId
    ];
  }

  path () {
    return '/user-b';
  }

  async handle (req, res) {
    return res.status(200).json({ id: req.query.id, name: 'User B' });
  }
}
```

#### Using Route.create

```js
const Route = require('@letumfalx/expressance/Route');
const validateId = require('./middlewares/validateId');

const UserRoute = Route.create(route => {
  route.path = () => '/user-a';
  route.method = () => 'get';
  route.middlewares = () => [ validateId ];
  route.handle = async (req, res) => res.status(200).json({ id: req.query.id, name: 'Admin' });
});
```

### Kernel

```js
const Kernel = require('@letumfalx/expressance/Kernel');
const Route = require('@letumfalx/expressance/Route');
const convertToNumber = require('./middlewares/convertToNumber');
const validateId = require('./middlewares/validateId');
const UserRoute = require('./UserRoute');

new Kernel()
  // add the global middlewares
  .cookieParser()
  .cors()
  .json()
  .raw()
  .text()
  .trimRequest()
  .urlencoded()
  .addMiddleware(convertToNumber)
  // add the routes
  // using Route.create
  .addRoute(Route.create(route => {
    route.path = () => '/user-a';
    route.method = () => 'get';
    route.middlewares = () => [ validateId ];
    route.handle = async (req, res) => res.status(200).json({ id: req.query.id, name: 'Admin' });
  }))
  // using class
  .addRoute(new UserRoute())
  .start(12205, () => console.log('Server is running!'));

```

## License

MIT
