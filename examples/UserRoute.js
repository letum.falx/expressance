const Route = require('../Route');
const validateId = require('./middlewares/validateId');

class UserRoute extends Route {
  method () {
    return 'get';
  }

  middlewares () {
    return [
      validateId
    ];
  }

  path () {
    return '/user-b'
  }

  async handle (req, res) {
    return res.status(200).json({ id: req.query.id, name: 'User B' });
  }
}

module.exports = UserRoute;
