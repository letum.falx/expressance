const Kernel = require('../Kernel');
const Route = require('../Route');
const convertToNumber = require('./middlewares/convertToNumber');
const validateId = require('./middlewares/validateId');
const UserRoute = require('./UserRoute');

new Kernel()
  .cookieParser()
  .cors()
  .json()
  .raw()
  .text()
  .trimRequest()
  .urlencoded()
  .addMiddleware(convertToNumber)
  .addRoute(Route.create(route => {
    route.path = () => '/user-a';
    route.method = () => 'get';
    route.middlewares = () => [ validateId ];
    route.handle = async (req, res) => res.status(200).json({ id: req.query.id, name: 'Admin' });
  }))
  .addRoute(new UserRoute())
  .start(12205, () => console.log('Server is running!'));
