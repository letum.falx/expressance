const convert = obj => {
  if (obj && typeof obj === 'object') {
    for (const key in obj) {
      const temp = parseFloat(obj[key]);
      if (!isNaN(temp)) {
        obj[key] = temp;
      }
    }
  }

  return obj;
};

/**
 * This will convert anything that can be parsed to
 * float into number at first level.
 *
 * @type {import('express').RequestHandler}
 */
const convertToNumber = async (req, res, next) => {
  req.body = convert(req.body);
  req.cookies = convert(req.cookies);
  req.query = convert(req.query);
  req.params = convert(req.params);

  await next();
};

module.exports = convertToNumber;
