/**
 * Validates if the given query is numeric 1.
 *
 * @type {import('express').RequestHandler}
 */
const validateId = async (req, res, next) => {
  const id = req.query.id;
  if (id !== 1) {
    return res.status(404).json({ message: 'User not found.' });
  }

  next();
};

module.exports = validateId;
