const babelify = require('./babelify');
const cleanFiles = require('./cleanFiles');

cleanFiles()
  .then(babelify);
