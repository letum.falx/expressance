const path = require('path');
const fs = require('fs').promises;
const babel = require('@babel/core');

const { outputPath, sourcePath } = require('./config');
const cleanFiles = require('./cleanFiles');
const fileList = require('./fileList');



/**
 * Babelify and save the result to the output directory.
 *
 * @param {String} fullPathName the full path name of the file to babelify
 */
const babelify = async fullPathName => {
  const outputFile = path.resolve(outputPath, path.relative(sourcePath, fullPathName));
  return await fs.mkdir(path.dirname(outputFile), { recursive: true })
    .then(async () => await babel.transformFileAsync(fullPathName))
    .then(async ({ code }) => await fs.writeFile(outputFile, code));
};

/**
 * Copy the file to the output directory.
 *
 * @param {String} fullPathName the full path name of the file to copy
 */
const copyFile = async fullPathName => {
  const outputFile = path.resolve(outputPath, path.relative(sourcePath, fullPathName));
  return await fs.mkdir(path.dirname(outputFile), { recursive: true })
    .then(async () => await fs.copyFile(fullPathName, outputFile));
};

/**
 * @param {String} fileName
 */
const processFile = async fileName => await (/\.js$/.test(fileName) ? babelify : copyFile)(fileName);

module.exports = async () => {
  for (let i = 0; i < fileList.length; ++i) {
    await processFile(path.resolve(sourcePath, fileList[i]));
  }
};
