const path = require('path');
const { outputPath } = require('./config');
const fileList = require('./fileList');
const rimraf = require('./rimraf');


module.exports = async () => {
  for (let i = 0; i < fileList.length; ++i) {
    await rimraf(path.resolve(outputPath, fileList[i]));
  }
}
