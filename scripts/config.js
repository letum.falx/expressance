const path = require('path');

module.exports = {
  outputPath: path.resolve(__dirname, '..'),
  sourcePath: path.resolve(__dirname, '..', 'src')
};
