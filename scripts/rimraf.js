const rimraf = require('rimraf');

module.exports = (path, options = {}) => new Promise((resolve, reject) => rimraf(path, options, error => error ? reject(error) : resolve(path)));
