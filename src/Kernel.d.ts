import { Server } from 'http';
import { ListenOptions } from 'net';
import {
  RequestHandler,
  Express
} from 'express';
import Route from './Route';
import { CorsOptions } from 'cors';
import { CookieParseOptions } from 'cookie-parser';
import {
  OptionsJson,
  OptionsUrlencoded,
  OptionsText,
  Options as OptionsRaw
} from 'body-parser';

class Kernel {
  /**
   * The underlying express application.
   */
  public get app (): Express;

  /**
   * The underlying http server instance.
   */
  public get server (): Server;

  /**
   * Add an express middleware as a global middleware.
   *
   * @param middleware the middleware to add
   */
  public addMiddleware (middleware: RequestHandler): this;

  /**
   * Add a route.
   *
   * @param route the route to add
   */
  public addRoute (route: Route): this;

  /**
   * Add the cookie-parser as a global middleware.
   *
   * @param secret the secret to use for the cookie
   * @param options the options used to parse the incoming cookie
   */
  public cookieParser (secret?: String|String[], options?: CookieParseOptions): this;

  /**
   * Add the cors middleware as a global middleware.
   *
   * @param options the options to be passed to the cors middleware
   */
  public cors (options?: CorsOptions): this;

  /**
   * Add the json parser middleware as a global middleware.
   *
   * @param options the options to be passed to the json middleware
   */
  public json (options: OptionsJson): this;

  /**
   * Add the raw parser middleware as a global middleware.
   *
   * @param options the options to pass to the raw middleware
   */
  public raw (options: OptionsRaw): this;

  /**
   * Starts the server. This will just pass the given
   * arguments to the server's listen method.
   */
  public start (port?: number, hostname?: string, backlog?: number, listeningListener?: () => void): this;
  public start (port?: number, hostname?: string, listeningListener?: () => void): this;
  public start (port?: number, backlog?: number, listeningListener?: () => void): this;
  public start (port?: number, listeningListener?: () => void): this;
  public start (path: string, backlog?: number, listeningListener?: () => void): this;
  public start (path: string, listeningListener?: () => void): this;
  public start (options: ListenOptions, listeningListener?: () => void): this;
  public start (handle: any, backlog?: number, listeningListener?: () => void): this;
  public start (handle: any, listeningListener?: () => void): this;

  /**
   * Add the text parser middleware as a global middleware.
   *
   * @param options the options to be passed to the text middleware
   */
  public text (options: OptionsText): this;

  /**
   * Add the trim-request middleware as a global middleware.
   *
   * @param mode the type of data to trim
   */
  public trimRequest (type: 'all'|'body'|'param'|'query'): this;

  /**
   * Add the urlencoded parser middleware as a global middleware.
   *
   * @param options the options to pass to the middleware
   */
  public urlencoded (options: OptionsUrlencoded): this;

  /**
   * Wrap the function to an async function.
   *
   * @param callback the function to wrap
   */
  protected _wrapAsync (callback: RequestHandler): RequestHandler;

  /**
   * Check if the given method is available.
   *
   * @param method the method to validate
   * @returns the correct casing of the method
   */
  protected _validateAndReturnMethod (method: String): 'get'|'post'|'put'|'patch'|'delete';
}

export = Kernel;
