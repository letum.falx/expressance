const express = require('express');
const http = require('http');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const trimRequest = require('trim-request');
const Route = require('./Route');

class Kernel {
  constructor () {
    this._app = null;
    this._server = null;
  }

  get app () {
    if (!this._app) {
      this._app = express();
    }

    return this._app;
  }

  get server () {
    if (!this._server) {
      this._server = http.createServer(this.app);
    }

    return this._server;
  }

  addMiddleware (middleware) {
    this.app.use(this._wrapAsync(middleware));
    return this;
  }

  addRoute (route) {
    if (route instanceof Route) {
      const method = this._validateAndReturnMethod(route.method());
      this.app[method](route.path(), ...route._getHandlers().map(this._wrapAsync));
      return this;
    }

    throw new TypeError('route should be an instance of Route');
  }

  cookieParser (secret, options = {}) {
    return this.addMiddleware(cookieParser(secret, options));
  }

  cors (options = {}) {
    return this.addMiddleware(cors(options));
  }

  json (options) {
    return this.addMiddleware(express.json(options));
  }

  raw (options) {
    return this.addMiddleware(express.raw(options));
  }

  start (...args) {
    return this.server.listen(...args)
  }

  text (options) {
    return this.addMiddleware(express.text(options));
  }

  trimRequest (type = 'all') {
    const available = ['all', 'body', 'param', 'query'];
    if (!available.includes(type)) {
      throw new Error(`trim request invalid mode: ${type}`);
    }

    return this.addMiddleware(trimRequest[type]);
  }

  urlencoded (options = { extended: true }) {
    return this.addMiddleware(express.urlencoded(options));
  }

  _wrapAsync (callback) {
    return async (req, res, next) => await callback(req, res, next)
  }

  _validateAndReturnMethod(method) {
    const available = ['get', 'post', 'put', 'patch', 'delete'];
    method = method.toLowerCase();
    if (!available.includes(method)) {
      throw TypeError(`invalid http method: ${method}`);
    }

    return method;
  }
}

module.exports = Kernel;
