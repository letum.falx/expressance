import { RequestHandler } from 'express';

/**
 * An object for handling incoming HTTP request.
 * Note that this will be instantiated once for the
 * whole lifetime of the application. Saved data to
 * `this` will persist between requests, so do not
 * save request-level data to `this` as it will not
 * be reset on other request. But you can save utility
 * data or methods (like caching) to `this`.
 */
class Route {
  /**
   * Create a new generic route that will be modified
   * by the given modifier.
   *
   * @param modifier the callback used to modify the created route
   */
  public static create (modifier: (route: Route) => void): Route;

  /**
   * Get the method to be used for this route.
   */
  public method(): 'get'|'post'|'put'|'patch'|'delete';

  /**
   * Get the middlewares to be used in this route.
   */
  public middlewares(): RequestHandler[];

  /**
   * Get the path where this route will be bound.
   */
  public path(): String;

  /**
   * Handles the incoming request.
   */
  public handle: RequestHandler;

  /**
   * Get the sequence of request handlers for this route.
   * (middlewares + handle method)
   */
  protected _getHandlers(): RequestHandler[];
}

export = Route;
