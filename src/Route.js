class Route {
  static create(modifier) {
    const route = new Route();
    modifier(route);
    return route;
  }

  middlewares () {
    return [];
  }

  _getHandlers () {
    return [
      ...this.middlewares(),
      this.handle.bind(this)
    ];
  }


}

module.exports = Route;
